package com.waltsu.jayna;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class TextActivity extends Activity {

    private Camera camera;
    private CameraPreview cameraPreview;
    private MediaRecorder mediaRecorder;
    private PlaceholderFragment fragment;
    private  int song = 0;

    private static int frontCameraId = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text);

        fragment = new PlaceholderFragment();
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, fragment)
                    .commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Handler h = new Handler();
        if (checkCameraHardware(TextActivity.this)) {
            camera = getCameraInstance();
            if (camera != null) {
                cameraPreview = new CameraPreview(TextActivity.this, camera);
                fragment.getPreviewLayout().addView(cameraPreview);

                h.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mediaRecorder = new MediaRecorder();
                        if (prepareVideoRecorder()) {
                            mediaRecorder.start();
                        } else {
                            releaseMediaRecorder();
                        }
                    }
                }, 5000);
            }
        }
    }

    /** Check if this device has a camera */
    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    /** A safe way to get an instance of the Camera object. */
    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            Camera.CameraInfo info = new Camera.CameraInfo();
            int count = Camera.getNumberOfCameras();
            for (int i = 0; i < count; i++) {
                Camera.getCameraInfo(i, info);
                if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                    frontCameraId = i;
                    c = Camera.open(frontCameraId);
                    Camera.Parameters p = c.getParameters();
                    p.setRecordingHint(true);
                    c.setParameters(p);
                }
            }
            if (c == null)
                c = Camera.open();
        }
        catch (Exception e){
            Log.v("debug", "NO CAMERA!!");
        }
        return c; // returns null if camera is unavailable
    }

    private boolean prepareVideoRecorder(){
        mediaRecorder = new MediaRecorder();

        // Step 1: Unlock and set camera to MediaRecorder
        camera.unlock();
        mediaRecorder.setCamera(camera);

        // Step 2: Set sources
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

        // Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
        mediaRecorder.setProfile(CamcorderProfile.get(frontCameraId, CamcorderProfile.QUALITY_HIGH));

        // Step 4: Set output file
        Log.v("debug", "Setting output file");
        mediaRecorder.setOutputFile(getOutputMediaFile(MEDIA_TYPE_VIDEO).toString());

        Surface sv = cameraPreview.getHolder().getSurface();
        // Step 5: Set the preview output
        mediaRecorder.setPreviewDisplay(sv);

        mediaRecorder.setMaxFileSize(100 * 1000000);

        // Step 6: Prepare configured MediaRecorder
        try {
            mediaRecorder.prepare();
        } catch (IllegalStateException e) {
            Log.d("debug", "IllegalStateException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            Log.d("debug", "IOException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        }
        return true;
    }

    /** Create a file Uri for saving an image or video */
    private static Uri getOutputMediaFileUri(int type){
        return Uri.fromFile(getOutputMediaFile(type));
    }

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    /** Create a File for saving an image or video */
    private static File getOutputMediaFile(int type){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/jayna");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_"+ timeStamp + ".jpg");
        } else if(type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_"+ timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseMediaRecorder();       // if you are using MediaRecorder, release it first
        releaseCamera();              // release the camera immediately on pause event
    }

    private void releaseMediaRecorder(){
        if (mediaRecorder != null) {
            mediaRecorder.reset();   // clear recorder configuration
            mediaRecorder.release(); // release the recorder object
            mediaRecorder = null;
            camera.lock();           // lock camera for later use
        }
    }

    private void releaseCamera(){
        if (camera != null){
            camera.release();        // release the camera for other applications
            camera = null;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.text, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void nextSong(View v) {
        song++;
        switch (song) {
            case 1:
                fragment.setSong(getString(R.string.teuvo_title), getString(R.string.teuvo));
                break;
            case 2:
                fragment.setSong(getString(R.string.rafaelin_title), getString(R.string.rafaelin_enkeli));
                break;
            case 3:
                fragment.setSong(getString(R.string.joutsenlaulu_title), getString(R.string.joutsenlaulu));
                fragment.setNextButtonText("Analysoi");
                break;
            default:
                startAnalyze();
        }

    }
    public void startAnalyze() {
        startActivity(new Intent(this, FinalActivity.class));
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        private TextView lyrics;
        private Button nextButton;
        private FrameLayout previewLayout;
        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_text, container, false);

            nextButton = (Button) rootView.findViewById(R.id.nextButton);
            previewLayout = (FrameLayout) rootView.findViewById(R.id.camera_preview);

            lyrics = (TextView) rootView.findViewById(R.id.lyricsView);
            lyrics.setText(getString(R.string.eppu_normaali));
            getActivity().getActionBar().setTitle(getString(R.string.eppu_title));
            return rootView;
        }

        public void setSong(String title, String content) {
            if (getActivity() != null) {
                getActivity().getActionBar().setTitle(title);
                lyrics.setText(content);
            }
        }

        public void setNextButtonText(String text) {
            nextButton.setText(text);
        }

        public FrameLayout getPreviewLayout() {
            return previewLayout;
        }
    }

}
