package com.waltsu.jayna;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class FinalActivity extends Activity {

    private static final int ANALYZE_TIME = 2;

    PlaceholderFragment fragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final);

        fragment = new PlaceholderFragment();

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, fragment)
                    .commit();
        }

        getActionBar().setTitle("Analysoidaan...");

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                fragment.showResult();
            }
        }, ANALYZE_TIME * 1000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.final_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        private ProgressBar progressBar;
        private TextView resultText;

        private ArrayList<String> resultTexts;

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_final, container, false);
            progressBar = (ProgressBar) rootView.findViewById(R.id.analyzeProgress);
            resultText = (TextView) rootView.findViewById(R.id.analyzeText);
            return rootView;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            resultTexts = new ArrayList<String>();
            resultTexts.add(getString(R.string.result_1));
            resultTexts.add(getString(R.string.result_2));
            resultTexts.add(getString(R.string.result_3));
            resultTexts.add(getString(R.string.result_4));
            resultTexts.add(getString(R.string.result_5));
        }

        public void showResult() {
            progressBar.setVisibility(View.GONE);
            resultText.setVisibility(View.VISIBLE);
            if (getActivity() != null)
                getActivity().getActionBar().setTitle("Tulokset");

            Random rand = new Random();
            resultText.setText(resultTexts.get(rand.nextInt(5)));
        }
    }

}
